<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_cliente';

    protected $table = 'cliente';

    protected $fillable = ['nombre_cliente', 'telefono', 'email'];

    public $timestamps = false;

}
