<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_cliente' => 'required|max:50',
            'telefono' => 'required|numeric',
            'email' => 'required|email|unique:cliente,email',
        ];
    }

    public function messages()
    {
        return [
            'nombre_cliente.required' => 'El nombre es requerido',
            'telefono.required' => 'El telefono es requerido',
            'email.required' => 'El email es requerido',
            'telefono.numeric' => 'El telefono debe ser solo numeros',
            'email.email' => 'El email debe ser uno valido',
        ];
    }
}
