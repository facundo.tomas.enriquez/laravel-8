@extends('layouts.inspinia')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-7">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Nuevo Cliente</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    @if(session()->has('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{session()->get('message')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <form method="POST" action="{{ route('cliente.store') }}">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-2">Nombre</label>
                            <div class="col-sm-5"><input name="nombre" type="text" class="form-control @error('nombre') is-invalid
                            @enderror">
                                @error('nombre')
                                <span class="form-text text-danger m-b-none">{{ $message }}</span>
                                @enderror
                            </div>
                            <label class="col-sm-1 col-form-label">Telefono</label>
                            <div class="col-sm-5"><input name="telefono" type="text" class="form-control @error('telefono') is-invalid
                            @enderror">
                                @error('telefono')
                                <span class="form-text text-danger m-b-none">{{ $message }}</span>
                                @enderror
                            </div>
                            <label class="col-sm-1 col-form-label">Email</label>
                            <div class="col-sm-5"><input name="email" type="email" class="form-control @error('email') is-invalid
                            @enderror">
                                @error('email')
                                <span class="form-text text-danger m-b-none">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-sm-5 col-sm-offset-2">
                                <a href="{{ route('index') }}" class="btn btn-white btn-sm">Cancelar</a>
                                <button class="btn btn-primary btn-sm" type="submit">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection