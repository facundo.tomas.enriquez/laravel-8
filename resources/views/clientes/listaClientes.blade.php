@extends('layouts.inspinia')

@section('content')
<div class="row">
    <div class="col-lg-8">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Lista de clientes</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">

                <div class="table-responsive">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                        <table class="table table-striped table-bordered table-hover dataTables-example dataTable"
                            id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info" role="grid">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                        colspan="1" aria-sort="ascending"
                                        aria-label="Rendering engine: activate to sort column descending"
                                        style="width: 281px;">Nombre </th>
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                        colspan="1" aria-label="Browser: activate to sort column ascending"
                                        style="width: 346px;">Email</th>
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                        colspan="1" aria-label="Platform(s): activate to sort column ascending"
                                        style="width: 312px;">Telefono</th>
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                        colspan="1" aria-label="CSS grade: activate to sort column ascending"
                                        style="width: 174px;">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($clientes as $cliente)
                                <tr role="row">
                                    <td class="sorting_1">{{ $cliente->nombre_cliente }}</td>
                                    <td>{{ $cliente->telefono }}</td>
                                    <td class="center">{{ $cliente->email }}</td>
                                    <td class="center">
                                        <a href="#" data-toggle="modal"
                                            data-target="#editModal{{$cliente->id_cliente}}"><i
                                                class="fa fa-edit btn btn-primary"></a></i>
                                        <a href="#" data-toggle="modal"
                                            data-target="#deleteModal{{$cliente->id_cliente}}"><i
                                                class="fa fa-trash-o btn btn-danger"></a></i>
                                    </td>
                                </tr>

                                <div class="modal inmodal" id="editModal{{$cliente->id_cliente}}" tabindex="-1"
                                    role="dialog" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated fadeIn">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span
                                                        aria-hidden="true">×</span><span
                                                        class="sr-only">Close</span></button>
                                                <h4 class="modal-title">Editar cliente</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form
                                                    action="{{ route('cliente.update', [$cliente, $cliente->id_cliente]) }}"
                                                    method="post" id="editForm{{$cliente->id_cliente}}">
                                                    @csrf
                                                    @method('PUT')
                                                    <div class="form-group row mt-3">
                                                        <div class="form-group col-md-6">
                                                            <label>Nombre</label>
                                                            <input name="nombre" type="text"
                                                                value="{{ $cliente->nombre_cliente }}"
                                                                class="form-control @error('nombre') is-invalid @enderror">
                                                            @error('nombre')
                                                            <span
                                                                class="form-text text-danger m-b-none">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>Telefono</label>
                                                            <input name="telefono" type="text"
                                                                value="{{ $cliente->telefono }}"
                                                                class="form-control @error('telefono') is-invalid @enderror">
                                                            @error('telefono')
                                                            <span
                                                                class="form-text text-danger m-b-none">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>Email</label>
                                                            <input name="email" type="email"
                                                                value="{{ $cliente->email }}"
                                                                class="form-control @error('email') is-invalid @enderror">
                                                            @error('email')
                                                            <span
                                                                class="form-text text-danger m-b-none">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white"
                                                    data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" type="submit"
                                                    onclick="document.getElementById('editForm{{$cliente->id_cliente}}').submit()">Guardar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal inmodal" id="deleteModal{{$cliente->id_cliente}}" tabindex="-1"
                                    role="dialog" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated fadeIn">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span
                                                        aria-hidden="true">×</span><span
                                                        class="sr-only">Close</span></button>
                                                <h4 class="modal-title">Eliminar cliente</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{ route('cliente.destroy', [$cliente->id_cliente]) }}"
                                                    method="post" id="deleteForm{{$cliente->id_cliente}}">
                                                    @csrf
                                                    @method('DELETE')

                                                    <p>Esta seguro de eliminar el cliente {{$cliente->nombre_cliente}}?</p>

                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white"
                                                    data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" type="submit"
                                                    onclick="document.getElementById('deleteForm{{$cliente->id_cliente}}').submit()">Guardar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>@endsection